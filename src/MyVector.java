import java.util.Vector;

/**
 * Ali Ammar
 */
public class MyVector  extends Vector {
    MyVector(){
        super();
    }


    private <E> void swap(int i,int j) {
         E temp = (E)get(i);
        set(i,get(j));
        set(j,temp);
    }

    private <Q> void qs(int left, int right)  {
        MyComparator c = new MyComparator();
        Integer i = left, j = right;
        Q pivot = (Q) get((left + right) / 2);

        do {

            while (c.compare((Q)get(i),pivot) <0 && (i < right)) {
                i++;
            }
            while (c.compare(pivot,(Q) get(j))<0 && (j > left)) {
                j--;
            }
            if (i <= j) { swap(i++,j--); }
        } while (i <= j);
//         split and sort recursively
        if (left < j) qs(left, j);
        if (i < right) qs(i, right);
    }

    public void quickSorting() {
        qs(0, size()-1);
    }

}
