/*
* Ali Ammar
*/
public class Main {

    public static void main(String[] args) {
	   MyVector vector = new MyVector();
        vector.add("tycho");
        vector.add("dave");
        vector.add("ali");
        vector.add("tom");
        vector.add("rene");

        MyVector vector1 = new MyVector();
        vector1.add(5);
        vector1.add(8);
        vector1.add(1);
        vector1.add(6);

        vector.quickSorting();
        vector1.quickSorting();
        System.out.println(vector);
        System.out.println(vector1);
    }
}
